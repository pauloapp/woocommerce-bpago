<?php

/**
 * Part of Woo Bpago Module - BPago Integration Library, Access BPago for payments integration
 * Author - BPago
 * Developer - Emanuel Lima / emanuellima.bs@gmail.com
 * Copyright - Copyright(c) BPago [https://www.bpago.com]
 * License - https://www.gnu.org/licenses/gpl.html GPL version 2 or higher
 */

$GLOBALS['LIB_LOCATION'] = dirname( __FILE__ );

class BP {

	private $version = '1.0.0';
	private $client_id;
	private $client_secret;
	private $ll_access_token;
	private $API_BASE_URL = 'https://app.bpago.com.br';

	/**
	 * Summary: Constructor.
	 * Description: Build an object with module version and credentials.
	 */
	function __construct() {

		$i = func_num_args();

		if ( $i > 3 || $i < 2 ) {
			throw new BPagoException(
				'Invalid arguments. Use CLIENT_ID and CLIENT SECRET, or ACCESS_TOKEN'
			 );
		}

		if ( $i == 2 ) {
			$this->version = func_get_arg( 0 );
			$this->ll_access_token = func_get_arg( 1 );
		}

		if ( $i == 3 ) {
			$this->version = func_get_arg( 0 );
			$this->client_id = func_get_arg( 1 );
			$this->client_secret = func_get_arg( 2 );
		}
	}

	public function set_email( $email ) {
		BPRestClient::set_email( $email );
	}

	/**
	 * Summary: Get Access Token for API use.
	 * Description: Get Access Token for API use.
	 * @return a string that identifies the access token.
	 */
	public function get_access_token() {

		if ( isset( $this->ll_access_token ) && ! is_null( $this->ll_access_token ) ) {
			return $this->ll_access_token;
		}

		$app_client_values = array(
			'client_id' => $this->client_id,
			'client_secret' => $this->client_secret,
			'grant_type' => 'client_credentials'
		 );

		$access_data = BPRestClient::post(
			array(
				'uri' => '/oauth/token',
				'data' => $app_client_values,
				'headers' => array(
					'content-type' => 'application/x-www-form-urlencoded'
				 )
			 ),
			$this->version
		 );

		if ( $access_data['status'] != 200 ) {
			return null;
		}

		$access_data = $access_data['response'];

		return $access_data['access_token'];

	}

	/**
	 * Summary: Create a checkout preference.
	 * Description: Create a checkout preference.
	 * @param array $preference
	 * @return array( json )
	 */
	public function create_payment( $preference ) {

		$ch = curl_init();
        $url = $this->API_BASE_URL . '/api/v1/payments';

        $header = array(
        'Accept: application/json',
        'Authorization: Bearer '. $this->get_access_token()
        );

        curl_setopt($ch, CURLOPT_URL, $url );
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_USERAGENT, 'platform:v1-whitelabel,type:woocommerce,so:1.0.0' );
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, TRUE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, TRUE);
        curl_setopt($ch, CURLOPT_CAINFO, __DIR__.'/cacert.pem' );
		curl_setopt($ch, CURLOPT_CAPATH, __DIR__.'/cacert.pem' );

        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $this->build_query($preference));

        $result = curl_exec($ch);
        $api_http_code = curl_getinfo( $ch, CURLINFO_HTTP_CODE );

        $response = array (
            'status' => $api_http_code,
            'response' => json_decode( $result, true )
        );

        curl_close($ch);

		return $response;
	}

	public function retrieve_payment( $payment_id)
	{
		// use above token to make further api calls in this session or until the access token expires
        $ch = curl_init();
        $url = $this->API_BASE_URL . '/api/v1/payments/' . $payment_id;
        $header = array(
        'Accept: application/json',
        'Authorization: Bearer '. $this->get_access_token()
        );

        curl_setopt($ch,CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, TRUE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, TRUE);
        curl_setopt($ch, CURLOPT_CAINFO, __DIR__.'/cacert.pem' );
		curl_setopt($ch, CURLOPT_CAPATH, __DIR__.'/cacert.pem' );

        $result = curl_exec($ch);
        $api_http_code = curl_getinfo( $ch, CURLINFO_HTTP_CODE );

        $response = array (
            'status' => $api_http_code,
            'response' => json_decode( $result, true )
        );

        curl_close($ch);

        return $response;
	}

	function build_query( $params ) {

        if ( function_exists( 'http_build_query' ) ) {
            return http_build_query( $params, '', '&' );
        } else {
            foreach ( $params as $name => $value ) {
                $elements[] = "{$name}=" . urlencode( $value );
            }
            return implode( '&', $elements );
        }

    }
}

/**
 * MercadoPago cURL RestClient
 */
class BPRestClient {

	const API_BASE_URL = 'https://app.bpago.com.br';
	private static $email_admin = '';
	private static $site_locale = '';
	private static $check_loop = 0;

	private static function build_request( $request, $version ) {

		if ( ! extension_loaded ( 'curl' ) ) {
			throw new BPagoException( 'cURL extension not found. You need to enable cURL in your php.ini or another configuration you have.' );
		}

		if ( ! isset( $request['method'] ) ) {
			throw new BPagoException( 'No HTTP METHOD specified' );
		}

		if ( ! isset( $request['uri'] ) ) {
			throw new BPagoException( 'No URI specified' );
		}

		// Set headers
		$headers = array( 'accept: application/json' );

		$json_content = true;
		$form_content = false;
		$default_content_type = true;

		if ( isset( $request['headers'] ) && is_array( $request['headers'] ) ) {
			foreach ( $request['headers'] as $h => $v ) {
				$h = strtolower( $h );
				$v = strtolower( $v );
				if ( $h == 'content-type' ) {
					$default_content_type = false;
					$json_content = $v == 'application/json';
					$form_content = $v == 'application/x-www-form-urlencoded';
				}
				array_push ( $headers, $h . ': ' . $v );
			}
		}
		if ( $default_content_type ) {
			array_push( $headers, 'content-type: application/json' );
		}

		// Build $connect
		$connect = curl_init();

		curl_setopt(
			$connect,
			CURLOPT_URL,
			BPRestClient::API_BASE_URL . $request['uri'] );

		curl_setopt(
			$connect,
			CURLOPT_RETURNTRANSFER,
			TRUE );

		curl_setopt(
			$connect,
			CURLOPT_HTTPHEADER,
			$headers );

		curl_setopt(
			$connect,
			CURLOPT_CUSTOMREQUEST,
			$request['method'] );

		curl_setopt(
			$connect,
			CURLOPT_USERAGENT,
			'platform:v1-whitelabel,type:woocommerce,so:' . $version );

		curl_setopt(
			$connect,
			CURLOPT_SSL_VERIFYPEER,
			TRUE );

    	curl_setopt(
    		$connect,
    		CURLOPT_SSL_VERIFYHOST,
    		TRUE );

		curl_setopt(
			$connect,
			CURLOPT_CAINFO,
			__DIR__.'/cacert.pem' );

		curl_setopt(
			$connect,
			CURLOPT_CAPATH,
			__DIR__.'/cacert.pem' );


		// Set parameters and url
		if ( isset( $request['params'] ) && is_array( $request['params'] ) ) {
			if ( count( $request['params'] ) > 0 ) {
				$request['uri'] .= ( strpos( $request['uri'], '?' ) === false ) ? '?' : '&';
				$request['uri'] .= self::build_query( $request['params'] );
			}
		}


		// Set data
		if ( isset( $request['data'] ) ) {
			if ( $json_content ) {
				if ( gettype( $request['data'] ) == 'string' ) {
					json_decode( $request['data'], true );
				} else {
					$request['data'] = json_encode( $request['data'] );
				}
				if( function_exists( 'json_last_error' ) ) {
					$json_error = json_last_error();
					if ( $json_error != JSON_ERROR_NONE ) {
						throw new BPagoException(
							"JSON Error [{$json_error}] - Data: " . $request['data']
						 );
					}
				}
			} elseif ( $form_content ) {
				$request['data'] = self::build_query( $request['data'] );
			}
			curl_setopt( $connect, CURLOPT_POSTFIELDS, $request['data'] );
		}

		return $connect;

	}

	private static function exec( $request, $version ) {

		$response = null;
		$connect = self::build_request( $request, $version );
		$api_result = curl_exec( $connect );
		$api_http_code = curl_getinfo( $connect, CURLINFO_HTTP_CODE );
		if ( $api_result === FALSE ) {
			throw new BPagoException ( curl_error ( $connect ) );
		}

		if ( $api_http_code != null && $api_result != null ) {
			// A common response without error.
			$response = array (
				'status' => $api_http_code,
				'response' => json_decode( $api_result, true )
			);
		}

		// Error log API.
		if ( $response != null && $response['status'] >= 400 && self::$check_loop == 0 ) {
			try {
				self::$check_loop = 1;
				$message = null;
				$payloads = null;
				$endpoint = null;
				$errors = array();
				if ( isset( $response['response'] ) ) {
					if ( isset($response['response']['message'] ) ) {
						$message = $response['response']['message'];
					}
					if ( isset( $response['response']['cause'] ) ) {
						if ( isset( $response['response']['cause']['code'] ) && isset( $response['response']['cause']['description'] ) ) {
							$message .= ' - ' . $response['response']['cause']['code'] . ': ' . $response['response']['cause']['description'];
						} elseif ( is_array( $response['response']['cause'] ) ) {
							foreach ( $response['response']['cause'] as $cause ) {
								$message .= ' - ' . $cause['code'] . ': ' . $cause['description'];
							}
						}
					}
				}
				if ( $request != null ) {
					if ( isset( $request['data'] ) ) {
						if ( $request['data'] != null ) {
							$payloads = json_encode( $request['data'] );
						}
					}
					if ( isset( $request['uri'] ) ) {
						if ( $request['uri'] != null ) {
							$endpoint = $request['uri'];
						}
					}
				}
				// Send error.
				$errors[] = array(
					'endpoint' => $endpoint,
					'message' => $message,
					'payloads' => $payloads
				);
				self::sendErrorLog( $response['status'], $errors, $version );
			} catch ( Exception $e ) {
				throw new BPagoException( 'error to call API LOGS' . $e );
			}
		}

		self::$check_loop = 0;

		curl_close( $connect );

		return $response;
	}

	private static function sendErrorLog( $code, $errors, $version ) {
		$data = array(
			'code' => $code,
			'module' => 'WooCommerce',
			'module_version' => $version,
			'url_store' => $_SERVER['HTTP_HOST'],
			'errors' => $errors,
			'email_admin' => self::$email_admin,
			'country_initial' => self::$site_locale
		);
		$request = array(
			'uri' => '/api/v1/modules/log',
			'data' => $data
		);
		$result_response = BPRestClient::post( $request, $version );
		return $result_response;
	}

	private static function build_query( $params ) {

		if ( function_exists( 'http_build_query' ) ) {
			return http_build_query( $params, '', '&' );
		} else {
			foreach ( $params as $name => $value ) {
				$elements[] = "{$name}=" . urlencode( $value );
			}
			return implode( '&', $elements );
		}

	}

	public static function get( $request, $version ) {
		$request['method'] = 'GET';

		return self::exec( $request, $version );
	}

	public static function post( $request, $version ) {
		$request['method'] = 'POST';
		return self::exec( $request, $version );
	}

	public static function put( $request, $version ) {
		$request['method'] = 'PUT';

		return self::exec( $request, $version );
	}

	public static function delete( $request, $version ) {
		$request['method'] = 'DELETE';

		return self::exec( $request, $version );
	}

	public static function set_email( $email ) {
		self::$email_admin = $email;
	}

	public static function set_locale( $country_code ) {
		self::$site_locale = $country_code;
	}

	/**
	 * Write message to log file
	 * @param  string $function function that will write to file
	 * @param  string $message  the message to be logged
	 * @return null
	 */
	public static function write_log($function, $message) {
        if (true === WP_DEBUG) {
            if (is_array($message) || is_object($message)) {
                error_log(
                	'bpago' .
					' - [' . $function . ']: ' . print_r($message, true));
            } else {
                error_log(
                	'bpago' .
					' - [' . $function . ']: ' . $message);
            }
        }
    }

}

class BPagoException extends Exception {
	public function __construct( $message, $code = 500, Exception $previous = null ) {
		// Default code 500
		parent::__construct( $message, $code, $previous );
	}
}
