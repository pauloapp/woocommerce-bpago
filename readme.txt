=== Bpago WooCommerce ===
Contributors: emanuellima
Tags: pagamento, boleto, payment, bpago
Requires at least: 1.0.0
Tested up to: 1.0.0
Requires PHP: 5.6
Stable tag: 1.0.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Receba pagamentos de forma simples, rápida e segura com o BPago.

== Description ==
### Como Funciona o BPago.com ###

[youtube https://www.youtube.com/watch?v=_pmgOxg10s8]

https://www.bpago.com.br

### Compatibilidade ###

Compatível com as versões 2.5.x ou maior do WooCommerce.