<?php

/**
 * Part of Bpago Payment Gateway
 * Author - B Inovação e Tecnologia
 * Developer - Emanuel Lima / emanuel@bpago.com.br
 * Copyright - Copyright(c) bpago [https://www.bpago.com]
 * License - https://www.gnu.org/licenses/gpl.html GPL version 2 or higher
 */


?>
<div class="mp-box-inputs mp-line" >
	<label>
		<span class="mensagem-ticket">
			<div class="tooltip">
				<?php echo esc_html__( 'Note: Confirmation under payment approval.', 'woocommerce-bpago' ); ?>
				<span class="tooltiptext">
					<?php
						echo esc_html__( 'Click [Place order] button. The ticket will be generated and you will be redirected to print it.', 'woocommerce-bpago' );
						echo ' ';
						echo esc_html__( 'Important: The order will be confirmed only after the payment approval.', 'woocommerce-bpago' );
					?>
				</span>
			</div>
		</span>
	</label>
</div>

<fieldset id="ticket_checkout_fieldset" style="margin:-1px; background:white;">

	<div id="bpago-form-ticket" class="mp-box-inputs mp-line" >
		<div id="form-ticket">
			<div class="form-row" style="height:24px; margin-bottom: 12px;">
				<!--<div class="form-col-1"> </div>-->
				<div class="form-col-6">
					<input type="radio" name="bpago[docType]" class="bpago-docType"
						id="bpago-docType-fisica" value="CPF" style="width:24px; height:24px;" checked="checked">
						<?php echo esc_html__( 'Fisical Person', 'woocommerce-bpago' ); ?>
					</input>
				</div>
				<!--<div class="form-col-2"> </div>-->
				<div class="form-col-6">
					<input type="radio" name="bpago[docType]" class="bpago-docType"
						id="bpago-docType-juridica" value="CNPJ" style="width:24px; height:24px;">
						<?php echo esc_html__( 'Legal Person', 'woocommerce-bpago' ); ?>
					</input>
				</div>
				<!--<div class="form-col-1"> </div>-->
			</div>
			<div class="form-row">
				<div class="form-col-8" id="box-firstname">
					<label for="firstname" class="title-name" id="label-pf"><?php echo esc_html__( 'NAME', 'woocommerce-bpago' ); ?><em class="obrigatorio"> *</em></label>
					<label for="firstname" class="title-razao-social" style="display: none;" id="label-pj"><?php echo esc_html__( 'SOCIAL NAME', 'woocommerce-bpago' ); ?><em class="obrigatorio"> *</em></label>
					<input type="text" value="<?php echo $febraban['firstname']; ?>"
						id="bp-firstname" class="form-control-mine" name="bpago[firstname]">
					<span class="erro_febraban" data-main="#bp-firstname" id="bp_error_firstname"><?php echo esc_html__( 'You must inform you NAME', 'woocommerce-bpago' ); ?></span>
				</div>
				<div class="form-col-4" id="box-docnumber">
					<label for="cpfcnpj" class="title-cpf" id="title-cpf"><?php echo esc_html__( 'DOCUMENT', 'woocommerce-bpago' ); ?><em class="obrigatorio"> *</em></label>
					<label for="cpfcnpj" class="title-cnpj" style="display: none;" id="title-cnpj"><?php echo esc_html__( 'CNPJ', 'woocommerce-bpago' ); ?><em class="obrigatorio"> *</em></label>
					<input type="text" value="<?php echo $febraban['docNumber']; ?>"
						id="bp-cpfcnpj" class="form-control-mine" name="bpago[docNumber]" maxlength="18">
					<span class="erro_febraban" data-main="#cpfcnpj" id="bp_error_docNumber"><?php echo esc_html__( 'You must inform your DOCUMENT', 'woocommerce-bpago' ); ?></span>
				</div>
			</div>

			<div class="form-row">
				<div class="form-col-8">
					<label for="email"><?php echo esc_html__( 'EMAIL', 'woocommerce-bpago' ); ?><em class="obrigatorio"> *</em></label>
					<input type="text" value="<?php echo $febraban['email']; ?>"
						id="bp-email" class="form-control-mine" name="bpago[email]">
					<span class="erro_febraban" data-main="#address" id="bp_error_email"><?php echo esc_html__( 'You must inform your EMAIL', 'woocommerce-bpago' ); ?></span>
				</div>
				<div class="form-col-4">
					<label for="phonenumber"><?php echo esc_html__( 'TELEFONE', 'woocommerce-bpago' ); ?><em class="obrigatorio"> *</em></label>
					<input type="text" value="<?php echo $febraban['phonenumber']; ?>"
						id="bp-phonenumber" class="form-control-mine" name="bpago[phonenumber]">
					<span class="erro_febraban" data-main="#phonenumber" id="bp_error_phonenumber"><?php echo esc_html__( 'You must inform your PHONE NUMBER', 'woocommerce-bpago' ); ?></span>
				</div>
			</div>

			<div class="form-row">
				<div class="form-col-8">
					<label for="address"><?php echo esc_html__( 'ADDRESS', 'woocommerce-bpago' ); ?><em class="obrigatorio"> *</em></label>
					<input type="text" value="<?php echo $febraban['address']; ?>"
						id="bp-address" class="form-control-mine" name="bpago[address]">
					<span class="erro_febraban" data-main="#address" id="bp_error_address"><?php echo esc_html__( 'You must inform your ADDRESS', 'woocommerce-bpago' ); ?></span>
				</div>
				<div class="form-col-4">
					<label for="number"><?php echo esc_html__( 'NUMBER', 'woocommerce-bpago' ); ?><em class="obrigatorio"> *</em></label>
					<input type="text" value="<?php echo $febraban['number']; ?>"
						id="bp-number" class="form-control-mine" name="bpago[number]">
					<span class="erro_febraban" data-main="#number" id="bp_error_number"><?php echo esc_html__( 'You must inform your ADDRESS NUMBER', 'woocommerce-bpago' ); ?></span>
				</div>
			</div>

			<div class="form-row">
				<div class="form-col-4">
					<label for="address2"><?php echo esc_html__( 'COMPLEMENTO', 'woocommerce-bpago' ); ?><em class="obrigatorio"> *</em></label>
					<input type="text" value="<?php echo $febraban['address2']; ?>"
						id="bp-address2" class="form-control-mine" name="bpago[address2]">
					<span class="erro_febraban" data-main="#address2" id="bp_error_address2"><?php echo esc_html__( 'You must inform your ADDRESS2', 'woocommerce-bpago' ); ?></span>
				</div>
				<div class="form-col-4">
					<label for="neigh"><?php echo esc_html__( 'BAIRRO', 'woocommerce-bpago' ); ?><em class="obrigatorio"> *</em></label>
					<input type="text" value="<?php echo $febraban['neigh']; ?>"
						id="bp-neigh" class="form-control-mine" name="bpago[neigh]">
					<span class="erro_febraban" data-main="#neigh" id="bp_error_neigh"><?php echo esc_html__( 'You must inform your BAIRRO', 'woocommerce-bpago' ); ?></span>
				</div>
				<div class="form-col-4">
					<label for="zipcode"><?php echo esc_html__( 'ZIP', 'woocommerce-bpago' ); ?><em class="obrigatorio"> *</em></label>
					<input type="text" value="<?php echo $febraban['zipcode']; ?>"
						id="bp-zipcode" class="form-control-mine" name="bpago[zipcode]">
					<span class="erro_febraban" data-main="#zipcode" id="bp_error_zipcode"><?php echo esc_html__( 'You must inform your ZIP', 'woocommerce-bpago' ); ?></span>
				</div>

			</div>

			<div class="form-row">
				<div class="form-col-4">
					<label for="city"><?php echo esc_html__( 'CITY', 'woocommerce-bpago' ); ?><em class="obrigatorio"> *</em></label>
					<input type="text" value="<?php echo $febraban['city']; ?>"
						id="bp-city" class="form-control-mine" name="bpago[city]">
					<span class="erro_febraban" data-main="#city" id="bp_error_city"><?php echo esc_html__( 'You must inform your CITY', 'woocommerce-bpago' ); ?></span>
				</div>
				<div class="form-col-4">
					<label for="state"><?php echo esc_html__( 'STATE', 'woocommerce-bpago' ); ?><em class="obrigatorio"> *</em></label>
					<select name="bpago[state]" id="bp-state" class="form-control-mine" style="width: 100%;">
						<option value="" <?php if ($febraban['state'] == '') {echo 'selected="selected"';} ?>><?php echo esc_html__( 'Choose', 'woocommerce-bpago' ); ?></option>
						<option value="AC" <?php if ($febraban['state'] == 'AC') {echo 'selected="selected"';} ?>>Acre</option>
						<option value="AL" <?php if ($febraban['state'] == 'AL') {echo 'selected="selected"';} ?>>Alagoas</option>
						<option value="AP" <?php if ($febraban['state'] == 'AP') {echo 'selected="selected"';} ?>>Amapá</option>
						<option value="AM" <?php if ($febraban['state'] == 'AM') {echo 'selected="selected"';} ?>>Amazonas</option>
						<option value="BA" <?php if ($febraban['state'] == 'BA') {echo 'selected="selected"';} ?>>Bahia</option>
						<option value="CE" <?php if ($febraban['state'] == 'CE') {echo 'selected="selected"';} ?>>Ceará</option>
						<option value="DF" <?php if ($febraban['state'] == 'DF') {echo 'selected="selected"';} ?>>Distrito Federal</option>
						<option value="ES" <?php if ($febraban['state'] == 'ES') {echo 'selected="selected"';} ?>>Espírito Santo</option>
						<option value="GO" <?php if ($febraban['state'] == 'GO') {echo 'selected="selected"';} ?>>Goiás</option>
						<option value="MA" <?php if ($febraban['state'] == 'MA') {echo 'selected="selected"';} ?>>Maranhão</option>
						<option value="MT" <?php if ($febraban['state'] == 'MT') {echo 'selected="selected"';} ?>>Mato Grosso</option>
						<option value="MS" <?php if ($febraban['state'] == 'MS') {echo 'selected="selected"';} ?>>Mato Grosso do Sul</option>
						<option value="MG" <?php if ($febraban['state'] == 'MG') {echo 'selected="selected"';} ?>>Minas Gerais</option>
						<option value="PA" <?php if ($febraban['state'] == 'PA') {echo 'selected="selected"';} ?>>Pará</option>
						<option value="PB" <?php if ($febraban['state'] == 'PB') {echo 'selected="selected"';} ?>>Paraíba</option>
						<option value="PR" <?php if ($febraban['state'] == 'PR') {echo 'selected="selected"';} ?>>Paraná</option>
						<option value="PE" <?php if ($febraban['state'] == 'PE') {echo 'selected="selected"';} ?>>Pernambuco</option>
						<option value="PI" <?php if ($febraban['state'] == 'PI') {echo 'selected="selected"';} ?>>Piauí</option>
						<option value="RJ" <?php if ($febraban['state'] == 'RJ') {echo 'selected="selected"';} ?>>Rio de Janeiro</option>
						<option value="RN" <?php if ($febraban['state'] == 'RN') {echo 'selected="selected"';} ?>>Rio Grande do Norte</option>
						<option value="RS" <?php if ($febraban['state'] == 'RS') {echo 'selected="selected"';} ?>>Rio Grande do Sul</option>
						<option value="RO" <?php if ($febraban['state'] == 'RO') {echo 'selected="selected"';} ?>>Rondônia</option>
						<option value="RA" <?php if ($febraban['state'] == 'RA') {echo 'selected="selected"';} ?>>Roraima</option>
						<option value="SC" <?php if ($febraban['state'] == 'SC') {echo 'selected="selected"';} ?>>Santa Catarina</option>
						<option value="SP" <?php if ($febraban['state'] == 'SP') {echo 'selected="selected"';} ?>>São Paulo</option>
						<option value="SE" <?php if ($febraban['state'] == 'SE') {echo 'selected="selected"';} ?>>Sergipe</option>
						<option value="TO" <?php if ($febraban['state'] == 'TO') {echo 'selected="selected"';} ?>>Tocantins</option>
					</select>
					<span class="erro_febraban" data-main="#bp-state" id="bp_error_state"><?php echo esc_html__( 'You must inform your STATE', 'woocommerce-bpago' ); ?></span>
				</div>
				<div class="form-col-4">
					<label for="country"><?php echo esc_html__( 'COUNTRY', 'woocommerce-bpago' ); ?><em class="obrigatorio"> *</em></label>
					<input type="text" value="<?php echo $febraban['country']; ?>"
						id="bp-country" class="form-control-mine" name="bpago[country]">
					<span class="erro_febraban" data-main="#country" id="bp_error_country"><?php echo esc_html__( 'You must inform your COUNTRY', 'woocommerce-bpago' ); ?></span>
				</div>
			</div>
			<label>
				<span class="mensagem-febraban">
					<em class="obrigatorio">* </em>
					<div class="tooltip">
						<?php echo esc_html__( 'Needed informations', 'woocommerce-bpago' ); ?>
						<span class="tooltiptext">
							<?php echo esc_html__( 'Needed informations due to brazilian bank compliances numbers 3.461/09, 3.598/12 and 3.656/13 of the Central Bank of Brazil.', 'woocommerce-bpago' ); ?>
						</span>
					</div>
				</span>
			</label>
			
			<div style="background-color: #ff9f19;color: white;font-size: 11.0pt;font-weight: 600;">
				<p style="padding: 11px;">
					<?php echo esc_html__( 'Para pagamento nessa modalidade será acrescentado uma tarifa de serviço no valor de R$ ' . $valor_tarifa . '', 'woocommerce-bpago' ); ?>
				</p>
			</div>
			
		</div>

		<div>

			<div class="mp-box-inputs mp-line">
				<div class="mp-box-inputs mp-col-25">
					<div id="mp-box-loading">
					</div>
				</div>
			</div>

			<!-- utilities -->
			<div class="mp-box-inputs mp-col-100" id="bpago-utilities">
				<input type="hidden" id="site_id" value="<?php echo $site_id; ?>" name="bpago[site_id]"/>
				<input type="hidden" id="amountTicket" value="<?php echo $amount; ?>" name="valor"/>
				<input type="hidden" id="currency_ratioTicket" value="<?php echo $currency_ratio; ?>" name="bpago[currency_ratio]"/>
			</div>

		</div>
	</div>

</fieldset>

<script type="text/javascript">
	// document.getElementsByClassName("title-razao-social").style.display = 'none !important';
	var typePessoaFisica   = document.getElementById('bpago-docType-fisica');
	var typePessoaJuridica = document.getElementById('bpago-docType-juridica');

	typePessoaFisica.addEventListener("click", function (argument) {
		document.getElementById("label-pj").style.display = 'none';
		document.getElementById("label-pf").style.display = 'block';
		document.getElementById("title-cnpj").style.display = 'none';
		document.getElementById("title-cpf").style.display = 'block';

	});
	typePessoaJuridica.addEventListener("click", function (argument) {
		document.getElementById("label-pf").style.display = 'none';
		document.getElementById("label-pj").style.display = 'block';
		document.getElementById("title-cpf").style.display = 'none';
		document.getElementById("title-cnpj").style.display = 'block';
	});
</script>

