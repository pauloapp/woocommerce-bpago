<?php
/*
 * Plugin Name: WooCommerce Bpago Payment Gateway
 * Plugin URI: https://bitbucket.org/emanueluser/woocommerce-bpago
 * Description: Emita boletos de pagamento de forma rápida e segura usando bpago
 * Author: Bpago Dev Team
 * Author URI: https://bpago.com.br
 * Version: 1.0.0
 *
 */

/**
 * Load plugin text domain.
 *
 * Need to require here before test for PHP version.
 *
 * @since 3.0.1
 */
function wc_bpago_load_plugin_textdomain() {
	load_plugin_textdomain(	'woocommerce-bpago', false, dirname( plugin_basename( __FILE__ ) ) . '/i18n/languages/' );
}

add_action( 'init', 'wc_bpago_load_plugin_textdomain' );

/*
 * This include Bpago library SDK
 */
require_once dirname( __FILE__ ) . '/includes/sdk/lib/bpago.php';

 /*
 * This action hook registers our PHP class as a WooCommerce payment gateway
 */

function bpago_add_gateway_class( $gateways ) {
	$gateways[] = 'WC_Bpago_Gateway'; // your class name is here
	return $gateways;
}
add_filter( 'woocommerce_payment_gateways', 'bpago_add_gateway_class' );
 
/*
 * The class itself, please note that it is inside plugins_loaded action hook
 */
add_action( 'plugins_loaded', 'bpago_init_gateway_class' );

function bpago_init_gateway_class() {
 
	class WC_Bpago_Gateway extends WC_Payment_Gateway {

		// General constants.
		const VERSION = '1.0.0';
		const MIN_PHP = 5.6;
 
 		/**
 		 * Class constructor, more about it in Step 3
 		 */
 		public function __construct() {
 			
 			// payment gateway plugin ID
			$this->id = 'bpago';
			// bpago instance
			$this->bp = new BP(	'1.0.0',
				$this->get_option( 'client_id' ),
				$this->get_option( 'client_secret' )
			);
			$email = ( wp_get_current_user()->ID != 0 ) ? wp_get_current_user()->user_email : null;
			$this->bp->set_email( $email );
			// URL of the icon that will be displayed on checkout page near your gateway name 
			$this->icon = apply_filters('woocommerce_bpago_icon',
							    		 plugins_url( 'assets/images/bpago_icon.png', __FILE__ )); 
			// in case you need a custom credit card form
			$this->has_fields = true; 
			$this->method_title = 'Bpago Gateway';
			// will be displayed on the options page
			$this->method_description = '<img width="150" height="52" src="' .
							    			plugins_url( 'assets/images/bpago.svg', __FILE__ ) .
							    		'"><br><br><strong>' .
							    			__( 'A forma mais rápida e prática de receber pagamentos via boletos!', 'wc-gateway-bpago' ) .
							    		'</strong>'; 
		 
			// gateways can support subscriptions, refunds, saved payment methods
			$this->supports = array(
				'products'
			);
		 
			// Method with all the options fields
			$this->init_form_fields();
		 
			// Load the settings.
			$this->init_settings();
			$this->title = $this->get_option( 'title' );
			$this->description = $this->get_option( 'description' );
			$this->enabled = $this->get_option( 'enabled' );
			$this->testmode = 'yes' === $this->get_option( 'testmode' );
		 
			// This action hook saves the settings
			add_action( 'woocommerce_update_options_payment_gateways_' . $this->id, array( $this, 'process_admin_options' ) );	
				
			// Checkout updates.
			add_action( 'woocommerce_thankyou_' . $this->id, array( $this, 'bpago_payment_options'));

			// Callback for order update	
			add_action( 'woocommerce_api_'. strtolower( get_class($this) ), array( &$this, 'callback_handler' ) );

			// Used by IPN to process valid incomings.
			add_action( 'valid_bpago_request',	array( $this, 'successful_request' ));

	 		}

 		/**
 		 * Callback to update order status
 		 * @return null
 		 */
 		public function callback_handler() {

 			$this->write_log(
				__FUNCTION__,
				' - received _POST content: ' .
				json_encode( $_POST, JSON_PRETTY_PRINT|JSON_UNESCAPED_UNICODE )
			);

			$data = $_POST;

			if ( isset($data["data_id"]) && !empty($data["data_id"]) 
				&& isset($data["type"]) && !empty($data["type"])) {

				// Needed informations are present, so start process then.
				try {
					if ( $data['type'] == 'payment' ) {
	
						$result = $this->bp->retrieve_payment( $data['data_id'] );

						if ( ! is_wp_error( $result ) && 
							( $result['status'] == 200 || $result['status'] == 201 ) ) {

							if ( $result['response'] && 
								(isset($result['response']['data']) && 
									!empty($result['response']['data']))) {

								header( 'HTTP/1.1 200 OK' );
								do_action( 'valid_bpago_request', $result['response']['data'] );

							} else {
								$this->write_log(
									__FUNCTION__,
									'error when processing response data: ' .
									json_encode( $result['response'], JSON_PRETTY_PRINT|JSON_UNESCAPED_UNICODE )
								);
							}
						} else {
							$this->write_log(
								__FUNCTION__,
								'error when processing received data: ' .
								json_encode( $result, JSON_PRETTY_PRINT|JSON_UNESCAPED_UNICODE )
							);
						}
					}
				} catch ( bpagoException $ex ) {
					$this->write_log(
						__FUNCTION__,
						'bpagoException: ' .
						json_encode( $ex, JSON_PRETTY_PRINT|JSON_UNESCAPED_UNICODE )
					);
				}

			}
		}

		/**
		 * When a reques is valid, it is going to be processed here
		 * @param  array $data the response from server
		 * @return null       write to log
		 */
		public function successful_request( $data )
		{
			$this->write_log( __FUNCTION__, 'starting to process order update...' );

			$payment = $data['payment'];
			$transaction = $payment['client_transaction'];
			
			$order_key = $transaction['transaction_id'];
			if ( empty( $order_key ) ) {
				return;
			}

			$order = wc_get_order( $order_key );

			// Check if order exists.
			if ( ! $order ) {

				$this->write_log( __FUNCTION__, 
					'ERROR - Error processing order #'.$order_key . '. Order not found.');

				return;
			}

			$status = isset( $payment['status'] ) ? $payment['status'] : 'failed';

			if ( $status == 'LIBERADO_PARA_GERAR_PB' ) {
				// Set order status to processing
				$order->update_status( 'processing', sprintf( __( 'Boleto  %s %s pago.', 'woocommerce-bpago' ), get_woocommerce_currency(), $order->get_total() ) );
			} else {
				// Set order status to payment failed
				$order->update_status( 'failed', sprintf( __( 'Boleto não foi pago.', 'woocommerce-bpago' ) ) );
			}

			$this->write_log( __FUNCTION__, 
				'finishing to process order #'.$order_key . 'update. Final status:' . $status );

		}
 
		/**
 		 * Plugin options, we deal with it in Step 3 too
 		 */
 		public function init_form_fields(){
 
			$this->form_fields = array(
			'enabled' => array(
				'title'       => 'Habilitar/Desabilitar',
				'label'       => 'Habilitar pagamento via BPago',
				'type'        => 'checkbox',
				'description' => '',
				'default'     => 'yes'
			),
			'title' => array(
				'title'       => 'Titúlo',
				'type'        => 'text',
				'description' => 'This controls the title which the user sees during checkout.',
				'default'     => 'BPago',
				'desc_tip'    => true,
			),
			'description' => array(
				'title'       => 'Descrição',
				'type'        => 'textarea',
				'description' => 'This controls the description which the user sees during checkout.',
				'default'     => 'Pay with your credit card via our super-cool payment gateway.',
			),
			// 'testmode' => array(
			// 	'title'       => 'Test mode',
			// 	'label'       => 'Enable Test Mode',
			// 	'type'        => 'checkbox',
			// 	'description' => 'Place the payment gateway in test mode using test API keys. Disable SSL validation.',
			// 	'default'     => 'no',
			// 	'desc_tip'    => true,
			// ),
			'tarifa' => array(
				'title'       => 'Valor da Tarifa',
				'type'        => 'text',
				'description' => __( 'Caso o valor não seja inteiro, use \',\' 
						para separar os centavos. Ex: 2,55','woocommerce-bpago' ),

			),			
			'client_id' => array(
				'title'       => 'CLIENT_ID',
				'type'        => 'text',
				'description' => __( 'Insira seu BPago CLIENT_ID','woocommerce-bpago' ),

			),	
			'client_secret' => array(
				'title'       => 'CLIENT_SECRET',
				'type'        => 'text',
				'description' => __( 'Insira seu BPago CLIENT_SECRET', 'woocommerce-bpago' ),

			)
		);

	 	}
 
		/**
		 * You will need it if you want your custom credit card form, Step 4 is about it
		 */
		public function payment_fields() {
 
			$address = get_user_meta( wp_get_current_user()->ID, 'shipping_address_1', true );
			$address_2 = get_user_meta( wp_get_current_user()->ID, 'shipping_address_2', true );
			$amount = $amount = $this->get_order_total();

		 	$parameters = array(
		 		'amount' 				 => $amount,
		 		'woocommerce_currency'   => get_woocommerce_currency(),
				'account_currency'       => $this->site_data['currency'],
				'valor_tarifa'			 => $this->get_option( 'tarifa' ),
				// ===
				'febraban' => ( wp_get_current_user()->ID != 0 ) ?
					array(
						'firstname' => wp_get_current_user()->user_firstname . ' ' . wp_get_current_user()->user_lastname,
						'lastname' => wp_get_current_user()->user_lastname,
						'docNumber' => get_user_meta( wp_get_current_user()->ID, 'billing_cpf', true ),
						'address' => $address,
						'address2' => $address_2,
						'number' => get_user_meta( wp_get_current_user()->ID, 'billing_number', true ),
						'neigh' => get_user_meta( wp_get_current_user()->ID, 'billing_neighborhood', true ),
						'city' => get_user_meta( wp_get_current_user()->ID, 'shipping_city', true ),
						'state' => get_user_meta( wp_get_current_user()->ID, 'shipping_state', true ),
						'zipcode' => get_user_meta( wp_get_current_user()->ID, 'shipping_postcode', true ),
						'country' => get_user_meta( wp_get_current_user()->ID, 'shipping_country', true ),
						'phonenumber' => get_user_meta( wp_get_current_user()->ID, 'billing_phone', true ),
						'email' => wp_get_current_user()->user_email,

					) :
					array(
						'firstname' => '', 'lastname' => '', 'docNumber' => '', 'address' => '',
						'number' => '', 'city' => '', 'state' => '', 'zipcode' => '', 
						'phonenumber' => '', 'email' => '','address2' => '', 'country'=>'','neigh' => '',
					),

		 	);
		 	
			wc_get_template(
				'payment-form.php',
				$parameters,
				'',
				plugin_dir_path(__FILE__) . 'templates/'
			);
 
		}
 
		/*
 		 * Fields validation
		 */
		public function validate_fields() {
 
			if ( ! isset( $_POST['bpago'] ) ) {
				return;
			}

			$bpago_checkout = $_POST['bpago'];

			if ( ! isset( $bpago_checkout['firstname'] ) || empty( $bpago_checkout['firstname'] ) ||
				! isset( $bpago_checkout['docNumber'] ) || empty( $bpago_checkout['docNumber'] ) ||
				(strlen( $bpago_checkout['docNumber'] ) != 11 && strlen( $bpago_checkout['docNumber'] ) != 14 
				&& strlen( $bpago_checkout['docNumber'] ) != 18)||
				! isset( $bpago_checkout['address'] ) || empty( $bpago_checkout['address'] ) ||
				! isset( $bpago_checkout['number'] ) || empty( $bpago_checkout['number'] ) ||
				! isset( $bpago_checkout['city'] ) || empty( $bpago_checkout['city'] ) ||
				! isset( $bpago_checkout['state'] ) || empty( $bpago_checkout['state'] ) ||
				! isset( $bpago_checkout['zipcode'] ) || empty( $bpago_checkout['zipcode'] ) ) {
				wc_add_notice(
					'<p>' .
						__( 'A problem was occurred when processing your payment. Are you sure you have correctly filled all information in the checkout form?', 'woocommerce-bpago' ) .
					'</p>',
					'error'
				);
				return array(
					'result' => 'fail',
					'redirect' => '',
				);
			}

			return true;
		}
 
		/*
		 * We're processing the payments here
		 */
		public function process_payment( $order_id ) {

			global $woocommerce;		    

		    if ( ! isset( $_POST['bpago'] ) ) {
				return;
			}

			$order = wc_get_order( $order_id );

			$bpago_checkout = $_POST['bpago'];
			$bpago_checkout['order_id'] = $order_id;

			$response = $this->create_url( $order, $bpago_checkout );
			
			if ( array_key_exists( 'status', $response ) ) {

				if ( $response['status'] == 'DISPONIVEL_PARA_PAGAMENTO' ) {

				    // Mark as on-hold (we're awaiting the cheque)
				    $order->update_status('pending', __( 'Awaiting boleto payment', 'woocommerce_bpago' ));

				    // Reduce stock levels	  
					if ( method_exists( $order, 'reduce_order_stock' ) ) {
						$order->reduce_order_stock();
					} else {
						wc_reduce_stock_levels($order_id);
					}

				    // WooCommerce 3.0 or later.
					if ( method_exists( $order, 'update_meta_data' ) ) {
						$order->update_meta_data( '_transaction_details_bpago', $response['client_transaction']['resource_url'] );
						$order->save();
					} else {
						update_post_meta(
							$order->id,
							'_transaction_details_bpago',
							$response['client_transaction']['resource_url']
						);
					}

					// Shows some info in checkout page.
					$order->add_order_note(
						'BPago: ' .
						__( 'Customer haven\'t paid yet.', 'woocommerce-bpago' )
					);
					$order->add_order_note(
						'BPago: ' .
						__( 'To reprint the ticket click ', 'woocommerce-bpago' ) .
						'<a target="_blank" href="' .
						$response['client_transaction']['resource_url'] . '">' .
						__( 'here', 'woocommerce-bpago' ) .
						'</a>', 1, false
					);

				    // Remove cart
				    $woocommerce->cart->empty_cart();

				    // Return thankyou redirect
				    return array(
				        'result' => 'success',
				        'redirect' => $this->get_return_url( $order )
				    );

				}

			} else {
				// Process when fields are imcomplete.
				wc_add_notice(
					'<p>' .
						__( 'A problem was occurred when processing your payment. Are you sure you have correctly filled all information in the checkout form?', 'woocommerce-bpago' ) . ' BPAGO: ' .
						$response['error'] .
					'</p>',
					'error'
				);
				return array(
					'result' => 'fail',
					'redirect' => '',
				);
			}		    
 
	 	}

	 	/**
	 	 * Cria a URL pra
	 	 * @param  wp_oreder $order           [description]
	 	 * @param  array $ticket_checkout form fields
	 	 * @return array                  response from api
	 	 */
	 	protected function create_url( $order, $ticket_checkout ) {

			// Creates the order parameters by checking the cart configuration.
			$preferences = $this->build_payment_preference( $order, $ticket_checkout );
			// Create order preferences with Mercado Pago API request.
			// $this->write_log(
			// 			__FUNCTION__,
			// 			$preferences
			// 		);
			try {

				$checkout_info = $this->bp->create_payment( $preferences );

				if ( $checkout_info['status'] < 200 || $checkout_info['status'] >= 300 ) {
					// BPago throwed an error.
					if ( $checkout_info['response']['status'] == 'fail' ) {
						$this->write_log(
							__FUNCTION__,
							'bpago have failed, payment creation failed with error: ' 
							. $checkout_info['status'] . ' - ' . $checkout_info['response']['data']
						);

						return $checkout_info['response']['data'];
					} else {
						$this->write_log(
							__FUNCTION__,
							'bpago gave error, payment creation failed with error: ' 
							. $checkout_info['status'] . ' - ' . $checkout_info['response']['message']
						);
						return $checkout_info['response']['message'];
					}
					
				} elseif ( is_wp_error( $checkout_info ) ) {
					// WordPress throwed an error.
					$this->write_log(
						__FUNCTION__,
						'wordpress gave error, payment creation failed with error: ' . $checkout_info['response']['message']
					);

					return $checkout_info['response']['message'];

				} else {
					// Obtain the URL.
					$this->write_log(
						__FUNCTION__,
						'payment link generated with success from mercado pago, with structure as follow: ' .
						json_encode( $checkout_info, JSON_PRETTY_PRINT|JSON_UNESCAPED_UNICODE )
					);
					
					return $checkout_info['response']['data']['payments'][0];
					
				}
			} catch ( BPagoException $ex ) {
				// Something went wrong with the payment creation.
				$this->write_log(
					__FUNCTION__,
					'payment creation failed with exception: ' .
					json_encode( $ex, JSON_PRETTY_PRINT|JSON_UNESCAPED_UNICODE )
				);
				return $ex->getMessage();
			}
		}

	 	/**
		* Summary: Build BPago preference.
		* Description: Create BPago preference and get init_point URL based in the order options
		* from the cart.
		* @return the preference object.
		*/
		private function build_payment_preference( $order, $ticket_checkout ) {

			// Valor da tarifa de serviço
			$tarifa = floatval(str_replace(',', '.', $this->get_option( 'tarifa' )));

			$valor = number_format(($order->get_total() + $tarifa), 2, '.', '');

			$preferences = array(
				'valor' 	  	=> $valor,
				'vencimento'   	=> date('Y-m-d', strtotime('tomorrow')),
				'recorrencia' 	=> 'O',
				'quantidade'  	=> '1',
				'juros'		  	=> '000',
				'multa'		  	=> '000',
				'nome'		  	=> $ticket_checkout['firstname'],
				'num_documento' => $ticket_checkout['docNumber'],
				'email' 		=> $ticket_checkout['email'],
				'celular' 		=> $ticket_checkout['phonenumber'],
				'endereco'		=> array(
					'rua' 		  => $ticket_checkout['address'],
					'numero' 	  => $ticket_checkout['number'],
					'complemento' => $ticket_checkout['address2'],
					'bairro' 	  => $ticket_checkout['neigh'],
					'cidade' 	  => $ticket_checkout['city'],
					'uf' 		  => $ticket_checkout['state'],
					'cep' 		  => $ticket_checkout['zipcode'],
					'pais' 		  => $ticket_checkout['country'],
				),
				'transaction_details' => array(
					'transaction_id'	=> $order->get_order_number(),
					'callback_url'	=> get_site_url() . '/?wc-api=wc_bpago_gateway',
				),
			);

			return $preferences;
		}

	 	/**
	 	 * Exibe um iframe com o boleto a ser impresso 
	 	 * @param  int $order_id id da ordem a ser gerada o boleto
	 	 * @return null           echo na tela
	 	 */
	 	public function bpago_payment_options($order_id) {

	 		$order = wc_get_order( $order_id );
		
			$transaction_details = ( method_exists( $order, 'get_meta' ) ) ?
				$order->get_meta( '_transaction_details_bpago' ) :
				get_post_meta( $order->id, '_transaction_details_bpago', true );

			// A watchdog to prevent operations from other gateways.
			if ( empty( $transaction_details ) ) {
				return;
			}
			
			$html = '<p>' .
				__( 'Thank you for your order. Please, pay the ticket to get your order approved.', 'woocommerce-bpago' ) .
			'</p>' .
			'<p><iframe src="' . $transaction_details . '" style="width:100%; height:1000px;"></iframe></p>' .
			'<a id="submit-payment" target="_blank" href="' . $transaction_details . '" class="button alt"' .
			' style="font-size:1.25rem; width:100%; height:48px; line-height:50px; text-align:center;">' .
				__( 'Imprimir Boleto', 'woocommerce-bpago' ) .
			'</a> ';
			$added_text = '<p>' . $html . '</p>';
			echo $added_text;
		}

		/**
		 * Write message to log file
		 * @param  string $function function that will write to file
		 * @param  string $message  the message to be logged
		 * @return null          
		 */
		public function write_log($function, $message) {
	        if (true === WP_DEBUG) {
	            if (is_array($message) || is_object($message)) {
	                error_log(
	                	$this->id . 
						' - [' . $function . ']: ' . print_r($message, true));
	            } else {
	                error_log(
	                	$this->id . 
						' - [' . $function . ']: ' . $message);
	            }
	        }
	    }
 
 	}
}