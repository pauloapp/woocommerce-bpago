![Bpago](https://bitbucket.org/emanueluser/woocommerce-bpago/raw/238f1aa7b157f3058b8cfa83122b3e66ac002f3e/assets/images/bpago.png)

# Descrição

Este módulo permite que o WooCommerce use o BPago como método de pagamento para compras feitas em sua loja de comércio eletrônico. Esse plugin tem como objetivo trazer a melhor experiência para seu checkout, oferecendo o boleto bpago como forma rápida, eficiente e segura para receber seus pagamentos.

# Requisitos de Sistema

* WordPress 3.1.x - 5.0.x
* WooCommerce 2.6.x - 3.5.x
* LAMP (Linux, Apache, MySQL, e PHP)
* Linux x86
* Apache 2.x
* Deve poder sobrescrever o arquivo .htaccess
* PHP 5.6 ou maior, com suporte a cURL
* MySQL versão 5.6 ou maior OU MariaDB versão 10.0 ou maior
* WooCommerce
* safe_mode off
* memory_limit maior que 256MB (512MB é o recomendado)
* Certificação SSL é um pré-requisito para a emissão dos boletos

# Instalação

* Obtenha o código fonte do módulo do repositório [Bitbucket](https://bitbucket.org/emanueluser/woocommerce-bpago/get/master.zip).
* Descompacte a pasta e renomeie-a para "woocommerce-bpago".
* Copie o diretório "woocommerce-bpago" para dentro do diretório [WordPressRootDirectory]/wp-content/plugins/. Está feito!
* Para confirmar que seu módulo está realmente instalado, você pode clicar no item Plugins no menu lateral da página administrativa da sua loja, e checar seu módulo recém-instalado. Apenas clique em ativar (Imagem abaixo) para ativá-lo e você deverá receber a mensagem "Plugin ativado." como uma notificação em seu WordPress.

#Configuração

Na administração da sua loja, vá para a guia WooCommerce > Configurações > Payments. Em Opções de Checkout, clique em BPago Gateway. Você deve obter a seguinte página:

Esta janela mostra as principais configurações do plugin WooCommerce BPago, onde você pode verificar e configurar o seguinte:

* Título: Aqui você pode alterar o título do plugin caso deseje.
* Descrição: Aqui você pode alterar a descrição do plugin
* CLIENT_ID E CLIENT_SECRET: Aqui você deve colocar suas chaves CLIENT_ID e CLIENT_SECRET, que são as credenciais que o identificam de maneira única no BPago. [Clique aqui](https://app.bpago.com.br/user/account/credentials) para fazer acessar o sistema e obter suas credenciais.

#Suporte Técnico

Se você tiver dúvidas, problemas ou erros, temos um canal de suporte. Envie um email para suporte@binovacao.com.br com as seguintes informações:
* Email da sua conta BPago;
* Detalhes sobre sua pergunta, problema ou erro;
* Arquivos que podem ajudar na compreensão (Print-Screen, Video, Arquivos de Log, etc.);
* Versão do WooCommerce;
* Versão do WordPress

#Créditos

Este plugin foi baseado no Plugin para WooCommerce do Mercado Pago, disponívél [Aqui](https://github.com/mercadopago/cart-woocommerce)
